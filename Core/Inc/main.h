/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */


#define FUNC_SUCCESS	"\x1B[33mSUCCESS"
#define FUNC_FAIL		"\x1B[31mFAIL"
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define PHASEA_Pin GPIO_PIN_0
#define PHASEA_GPIO_Port GPIOC
#define PHASEA_EXTI_IRQn EXTI0_IRQn
#define PHASEB_Pin GPIO_PIN_1
#define PHASEB_GPIO_Port GPIOC
#define PHASEB_EXTI_IRQn EXTI1_IRQn
#define PULSE_Pin GPIO_PIN_2
#define PULSE_GPIO_Port GPIOC
#define PULSE_EXTI_IRQn EXTI2_IRQn
#define PHASEC_Pin GPIO_PIN_3
#define PHASEC_GPIO_Port GPIOC
#define PHASEC_EXTI_IRQn EXTI3_IRQn
#define OUT1_Pin GPIO_PIN_2
#define OUT1_GPIO_Port GPIOA
#define PWRKEY_Pin GPIO_PIN_7
#define PWRKEY_GPIO_Port GPIOC
#define PWR_GPS_Pin GPIO_PIN_15
#define PWR_GPS_GPIO_Port GPIOA
#define PWR_GSM_Pin GPIO_PIN_9
#define PWR_GSM_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart3;
extern UART_HandleTypeDef huart4;
extern DMA_HandleTypeDef hdma_uart4_rx;

extern TIM_HandleTypeDef htim5;
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
