#include <stdio.h>
#include "mqtt.h"
#include "FreeRTOS.h"
#include "task.h"

static MQTT_t userMQTTObj[MAX_SOCKET_INDEX] = {0};
static MQTTContext_t MQTTContextObj[MAX_SOCKET_INDEX] = {0};
static MQTTFixedBuffer_t fixedBuffer[MAX_SOCKET_INDEX] = {0};
static uint8_t buffer[1024];
static MQTT_p userMqttProc = {

};
static eventCallback callback = NULL;
static uint32_t getTimeStampMs(void)
{
    return xTaskGetTickCount();
}
static void coreMqttEventCallback(
       MQTTContext_t * pContext,
       MQTTPacketInfo_t * pPacketInfo,
       MQTTDeserializedInfo_t * pDeserializedInfo)
{
    MQTTStatus_t status = MQTTSuccess;
    uint8_t * pCodes;
    size_t numCodes;
    int index = pContext->transportInterface.pNetworkContext->tcpSocketCtx->index;
    uint8_t type = pPacketInfo->type  & 0xF0;
    // Logi("mqtt","event %d", type );
    if( type == MQTT_PACKET_TYPE_SUBACK )
    {
        status = MQTT_GetSubAckStatusCodes( pPacketInfo, &pCodes, &numCodes );
        if (status != MQTTSuccess) return;
        MQTTEvent_t event;
        event.event_id = MQTT_EVENT_SUBSCRIBED;
        event.client = &userMQTTObj[index];
        for( int i = 0; i < numCodes; i++ )
        {
            event.topic_idx = i;
            event.error_code = pCodes[i];
            if (callback != NULL) callback(&event);
        }
    }
    if (type == MQTT_PACKET_TYPE_PUBLISH)
    {
        MQTTEvent_t event;
        event.topic_len = pDeserializedInfo->pPublishInfo->topicNameLength;
        event.data_len = pDeserializedInfo->pPublishInfo->payloadLength;
        event.event_id = MQTT_EVENT_DATA;
        event.topic = (char*) malloc(event.topic_len + 1);
        event.data = (char*) malloc(event.data_len + 1);
        memset(event.topic, 0, event.topic_len + 1);
        memset(event.data, 0, event.data_len + 1);
        memcpy(event.topic, pDeserializedInfo->pPublishInfo->pTopicName, event.topic_len);
        memcpy(event.data, pDeserializedInfo->pPublishInfo->pPayload, event.data_len);
        if (callback != NULL) callback(&event);
    }
    if (type == MQTT_PACKET_TYPE_PUBACK || type == MQTT_PACKET_TYPE_PUBCOMP)
    {
    	MQTTEvent_t event;
    	event.event_id = MQTT_EVENT_PUBLISHED;
    	if (type == MQTT_PACKET_TYPE_PUBACK)
    		event.qos = 1;
    	else if (type == MQTT_PACKET_TYPE_PUBCOMP)
    		event.qos = 2;
    	if (callback != NULL)
    		callback(&event);
    }
    if (type == MQTT_PACKET_TYPE_DISCONNECT)
    {
        Logi("info","Disconnected");
    }
}
MQTT_t* MQTTInit(TransportInterface_t * interface)
{
    MQTT_t *obj = NULL;
    int index = interface->pNetworkContext->tcpSocketCtx->index;
    obj = &userMQTTObj[index];
    fixedBuffer[index].pBuffer = buffer;
    fixedBuffer[index].size = sizeof(buffer);
    MQTTStatus_t status = MQTT_Init(&MQTTContextObj[index], interface, getTimeStampMs, coreMqttEventCallback, &fixedBuffer[index]);
    if (status != MQTTSuccess)
        return NULL;
    obj->context = &MQTTContextObj[index];
    obj->proc = &userMqttProc;
    return obj;
}
MQTTStatus_t MQTTStart(MQTT_t* client, MQTTConnectInfo_t* info)
{
    if (!client)
        return MQTTBadParameter;
    uint32_t timeout = 2000;
    bool sessionPresent = false;
    MQTTEvent_t event = {0};
    client->info = info;
    MQTTStatus_t status = MQTT_Connect(client->context, client->info, NULL, timeout, &sessionPresent);
    event.client = client;
    event.event_id = MQTT_EVENT_CONNECTED;
    event.error_code = status;
    if (callback != NULL) callback(&event);
    return status;
}
MQTTStatus_t MQTTSubcribe(MQTT_t* client, const MQTTSubscribeInfo_t *pSubscriptionList, size_t subscriptionCount)
{
    if (!client)
        return MQTTBadParameter;
    uint16_t packetID = MQTT_GetPacketId(client->context);
    MQTTStatus_t status = MQTT_Subscribe(client->context, pSubscriptionList, subscriptionCount, packetID);
    return status;
}
MQTTStatus_t MQTTPublish(MQTT_t* client, const char *topic, const char * payload, int len, int qos, int retain)
{
    if (!client)
        return MQTTBadParameter;
    MQTTPublishInfo_t publishInfo = {
        .pTopicName = topic,
        .topicNameLength = strlen(topic),
        .pPayload = (void*) payload,
        .qos = qos,
        .retain = retain,
        .dup = 0,
    };
    if (len == 0) publishInfo.payloadLength = strlen(payload);
    else publishInfo.payloadLength = len;
    MQTTStatus_t status = 0;
    uint16_t packetID = MQTT_GetPacketId(client->context);
    status = MQTT_Publish(client->context, &publishInfo, packetID);
    return status;
}
void MQTTProcess(MQTT_t* client)
{
    static uint32_t entryTimeMs[MAX_SOCKET_INDEX] = {0};
    TransportInterface_t *netif = &client->context->transportInterface;
    int index = netif->pNetworkContext->tcpSocketCtx->index;
    if (netif->pNetworkContext->tcpSocketCtx->connected)
    {
        entryTimeMs[index] = (uint32_t) xTaskGetTickCount();
        if (client->context->connectStatus == MQTTConnected)
            MQTT_ProcessLoop(client->context, 1000);
        else
            MQTTStart(client, client->info);
    }
    else
    {
        client->context->connectStatus = MQTTNotConnected;
        if (xTaskGetTickCount() - entryTimeMs[index] >= 5000)
        {
            MQTTEvent_t event;
            event.event_id = MQTT_EVENT_DISCONNECTED;
            event.error_code = -1;
            if (callback != NULL)
            	callback(&event);
            entryTimeMs[index] = (uint32_t) xTaskGetTickCount();
        }
    }
}
void MQTTRegisterCallback(eventCallback cb)
{
    callback = cb;
}
