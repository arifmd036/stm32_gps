/*
 * socket.h
 *
 *  Created on: Aug 24, 2020
 *      Author: ManhTH
 */

#ifndef SIMCOM_SOCKET_H_
#define SIMCOM_SOCKET_H_

#include "fifo.h"
#include "simcom.h"

#define MAX_SOCKET_INDEX	5

typedef struct socket_t socket_t;

typedef struct socket_p {
	int (*open)(socket_t* sock, const char* host, int port);
	int (*send)(socket_t* sock, uint8_t* buffer, int size);
	int (*read)(socket_t* sock, uint8_t* buffer, int size, unsigned int time_out);
	int (*close)(socket_t* sock);
} socket_p;

struct socket_t {
	const socket_p* proc;
	simcom_t* simcom;
	fifo_t * fifo;
	bool connected;
	int index;
	int error;
};

socket_t* socket_create_tcp(simcom_t* simcom, int index);
int socket_send(socket_t* socket, uint8_t* data, int size);
int socket_read(socket_t *socket, uint8_t *data, int size, unsigned int time_out);
int socket_open(socket_t* sock, const char* host, int port);
int socket_close(socket_t* socket);

#endif /* SIMCOM_SOCKET_H_ */
