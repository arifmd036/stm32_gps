/*
 * network.c
 *
 *  Created on: Jan 29, 2019
 *      Author: dongnx
 */
#include "main.h"
#include "network.h"
#include "log.h"

static const char *TAG = "network";
static network_p network_proc = {
	network_connect,
	network_disconnect,
	network_get_signal};

static network_t nw_object = {0};
static char network_ip[64] = {0};
static int network_convert_rssi_to_dbm(int rssi);
static int network_dns_parse_hotname_to_ip_packet(uint8_t *packet, int size, uint32_t *ip_addr);

network_t *network_create(simcom_t *simcom)
{
	network_t *object = &nw_object;
	object->proc = &network_proc;
	object->simcom = simcom;
	object->connected = false;
	object->operator_name = NULL;
	object->ip = NULL;
	object->signal = -1;
	return object;
}

void network_dispose(network_t *object)
{
}

int network_disconnect(network_t *object)
{
	uint8_t buffer[100];
	simcom_t *simcom = object->simcom;
	if (network_get_sock_state(object) == 1) // Network actived
	{
		if (simcom->proc->send_AT(simcom, "+NETCLOSE") >= 0) 
			if (simcom->proc->receive(simcom, buffer, 100, 2000) >= 0)
			{
				object->connected = false;
				Logi( "Network disconnected");
				return 1;
			}
	}
	return -1;
}

int network_connect(network_t *object, const char *apn, const char *user, const char *pwd)
{
	uint8_t buffer[100];
	simcom_t *simcom = object->simcom;

	object->error = 0;
	do
	{
		if (network_get_sock_state(object) != 1)	//network deactived
		{
			if (simcom->proc->send_AT(simcom, (const char *)"+NETOPEN") < 0)
				break;
			if (simcom->proc->receive(simcom, buffer, 100, 2000) < 0)
				break;
		}
		if (network_get_ip(object) == NULL)
		{
			object->error = 4;
			break;
		}
		Logi( "Network connected, IP %s", object->ip);
		object->connected = true;
	} while (0);
	return (object->connected) ? 0 : -1;
}

int network_get_signal(network_t *object)
{
	return network_convert_rssi_to_dbm(simcom_get_csq(object->simcom));
}

const char *network_get_operator_name(network_t *object)
{

	if ((object->operator_name == NULL) && (object->simcom->proc->send_AT(object->simcom, "+COPS?") >= 0))
	{
		uint8_t message[64];
		int message_size = object->simcom->proc->receive(object->simcom, message, 64, 2000);
		if ((message_size > 0) && (message_size < 64))
		{
			if (strstr((char *)message, "Viettel") != NULL)
				object->operator_name = "Viettel";
			if (strstr((char *)message, "MobiFone") != NULL || strstr((char *)message, "Mobifone") != NULL)
				object->operator_name = "MobiFone";
			if (strstr((char *)message, "VinaPhone") != NULL || strstr((char *)message, "VINAFONE"))
				object->operator_name = "VinaPhone";
		}
	}
	return (const char *)object->operator_name;
}

int network_get_sock_state(network_t *object)
{
	int retval = -1;
	if (object->simcom->proc->send_AT(object->simcom, "+NETOPEN?") >= 0)
	{
		uint8_t message[64];
		int message_size = object->simcom->proc->receive(object->simcom, message, 64, 3000);
		if ((message_size > 0) && (message_size < 64))
		{
			retval = atoi((char *)&message[10]);
			// Logi("sock state %d",retval);
		}
	}
	return retval;
}

int network_set_APN(network_t *object, char *apn, char *user, char *pwd)
{
	int status = -1;
	char cmd[32] = {0};
	sprintf(cmd, "+CSTT=\"%s\",\"%s\",\"%s\"", apn, user, pwd);
	int retry = 0;
	while (retry < AT_RETRY)
	{
		if (object->simcom->proc->send_AT(object->simcom, cmd) >= 0)
		{
			uint8_t message[64];
			int message_size = object->simcom->proc->receive_ex(object->simcom, "\r\nOK", 3000, message, 64);
			if ((message_size == 0))
			{
				status = 1;
				break;
			}
		}
		retry++;
		vTaskDelay(2000);
	}
	Logi("%s set APN %s", object->simcom->model, (status) ? FUNC_SUCCESS : FUNC_FAIL);
	return status;
}

const char *network_get_ip(network_t *object)
{
	simcom_t *simcom = object->simcom;
	int retry = 0;
	int status = -1;
	while (retry < AT_RETRY)
	{
		if (simcom->proc->send_AT(simcom, "+CIFSR") >= 0)
		{
			uint8_t message[64];
			int ret = object->simcom->proc->receive_ex(object->simcom, "AT+CIFSR\r\r\n", 3000, message, 64);
			char error[10] = {0};
			if (ret == 0)
			{
				memcpy(error, (char *)&message[11], 5);
				if (strcmp(error,"ERROR") != 0)
				{
					sprintf(network_ip, "%s", (char *)&message[11]);
					object->ip = network_ip;
					status = 1;
					break;
				}
			}
		}
		retry++;
		vTaskDelay(1000);
	}
	if (status == 1)
		Logi( "%s get IP %s : %s", object->simcom->model, FUNC_SUCCESS, network_ip);
	else
		Logi( "%s get IP %s", object->simcom->model, FUNC_FAIL);
	return (const char *)object->ip;
}
int network_bringup_connection(network_t *object)
{
	int status = -1;
	int retry = 0;
	while (retry < AT_RETRY)
	{
		if (object->simcom->proc->send_AT(object->simcom, "+CIICR") >= 0)
		{
			uint8_t message[64];
			int message_size =object->simcom->proc->receive_ex(object->simcom, "\r\nOK", 3000, message, 64);
			if ((message_size == 0))
			{
				status = 1;
				break;
			}
		}
		retry++;
		vTaskDelay(2000);
	}
	Logi("%s bringup connection %s", object->simcom->model, (status) ? FUNC_SUCCESS : FUNC_FAIL);
	return status;
}

int network_deactivate_gprs(network_t *object)
{
	int status = -1;
	int retry = 0;
	while (retry < AT_RETRY)
	{
		if (object->simcom->proc->send_AT(object->simcom, "+CIPSHUT") >= 0)
		{
			uint8_t message[64];
			int message_size =object->simcom->proc->receive_ex(object->simcom, "\r\nSHUT OK", 3000, message, 64);
			if ((message_size == 0))
			{
				status = 1;
				break;
			}
		}
		retry++;
		vTaskDelay(2000);
	}
	Logi("%s shutdown connection %s", object->simcom->model, (status) ? FUNC_SUCCESS : FUNC_FAIL);
	return status;
}

int network_get_gprs_status(network_t *object)
{
	int status = -1;
	int retry = 0;
	while (retry < AT_RETRY)
	{
		if (object->simcom->proc->send_AT(object->simcom, "+CGATT?") >= 0)
		{
			uint8_t message[64];
			int message_size = object->simcom->proc->receive(object->simcom, message, 64, 3000);
			if ((message_size > 0) && (message_size < 64))
			{
				status = atoi((char *)&message[8]);
				break;
			}

		}
		retry++;
		vTaskDelay(2000);
	}
	return status;
}
int network_set_gprs_status(network_t *object, bool state)
{
	int ret = -1;
	char cmd[16] = {0};
	sprintf((char*)cmd,"+CGATT=%d",state);
	int retry = 0;
	while (retry < AT_RETRY)
	{
		if (object->simcom->proc->send_AT(object->simcom, cmd) >= 0)
		{
			uint8_t message[64];
			int message_size = object->simcom->proc->receive_ex(object->simcom, "\r\nOK", 3000, message, 64);
			if (message_size == 0)
			{
				ret = 1;
				break;
			}

		}
		retry++;
		vTaskDelay(2000);
	}
	Logi("%s %s GPRS  %s", object->simcom->model, (state) ? "Attach":"Dettach", (ret) ? FUNC_SUCCESS : FUNC_FAIL);
	return ret;
}
int network_get_registration_status(network_t *object)
{
	int status = -1;
	int retry = 0;
	while (retry < AT_RETRY)
	{
		if (object->simcom->proc->send_AT(object->simcom, "+CGREG?") >= 0)
		{
			uint8_t message[64];
			int message_size = object->simcom->proc->receive(object->simcom, message, 64, 3000);
			if ((message_size > 0) && (message_size < 64))
			{
				int n = atoi((char *)&message[8]);
				int stat = atoi((char *)&message[10]);
				if (n == 0 && stat == 1)
				{
					status = 1;
					break;
				}

			}
		}
		retry++;
		vTaskDelay(2000);
	}
	return status;
}


int network_get_pd_status(network_t *object) // packet domain services
{
	int retval = -1;
	if (object->simcom->proc->send_AT(object->simcom, "+CGATT?") >= 0)
	{
		uint8_t message[64];
		int message_size = object->simcom->proc->receive(object->simcom, message, 64, 3000);
		// Logi("%s",message);
		if ((message_size > 0) && (message_size < 64))
		{
			retval = atoi((char *)&message[8]);
			// Logi("ret val %d",retval);
		}
	}
	return retval;
}
int network_get_pdp_context_status(network_t *object, int cid)
{
	int retval = -1;
	if ((cid < 1) || (cid > 16))
		return -1;
	if (object->simcom->proc->send_AT(object->simcom, "+CGACT?") >= 0)
	{
		uint8_t message[100];
		int message_size = object->simcom->proc->receive(object->simcom, message, 100, 2000);
		if ((message_size > 0) && (message_size < 64))
		{
			int idx = (cid - 1) * 13 + 10;
			retval = atoi((char *)&message[idx]);
			Logi( "%d-%d", cid, retval);
		}
	}
	return retval;
}

int network_dns_get_ip_by_hostname(network_t *object, uint32_t *ip_addr, const char *host_name)
{
	uint8_t packet[100] = {0};
	uint8_t *message = NULL;
	int message_size;
	sprintf((char *)packet, "+CDNSGIP=\"%s\"", host_name);

	if (object->simcom->proc->send_AT(object->simcom, (const char *)packet) >= 0)
	{
		memset(packet, 0, 100);
		message_size = object->simcom->proc->receive(object->simcom, packet, 100, 10000);

		if ((message_size > 0) && (message_size < 100))
		{
			message = (uint8_t *)strstr((const char *)packet, "+CDNSGIP: 1");
		}

		if (message != NULL)
		{
			message += 12;
			message_size -= 12;
			return network_dns_parse_hotname_to_ip_packet(message, message_size, ip_addr);
		}
	}
	return (-1);
}

int network_dns_get_hostname_by_ip(network_t *object, uint32_t ip_addr, const char *host_name)
{
	return (-1);
}

static int network_convert_rssi_to_dbm(int rssi)
{
	int reval;

	if (rssi == 0)
		reval = -113;
	else if (rssi == 1)
		reval = -111;
	else if ((rssi >= 2) && (rssi <= 30))
		reval = (rssi * 2 - 113);
	else if (rssi == 31)
		reval = -51;
	else
		reval = -1;
	return reval;
}

static int network_dns_parse_hotname_to_ip_packet(uint8_t *packet, int size, uint32_t *ip_addr)
{
	//	int count = 0;
	//	while (count < size) {
	//		if (packet[0] == ',') break;
	//		packet++;
	//		count++;
	//	}
	//
	//	if (count < size) {
	//		uint8_t ip_string[16] = { 0 };
	//		packet += 2;
	//		size -= 2;
	//
	//		count = 0;
	//		while (count < size) {
	//			if (packet[count] == '"') break;
	//			ip_string[count] = packet[count];
	//			count++;
	//		}
	//
	//		if ((count > 0) && (count < size)) {
	//			*ip_addr = inet_addr((const char*)ip_string);
	//			return 0;
	//		}
	//	}
	return (-1);
}
