/*
 * simcom.c
 *
 *  Created on: Dec 6, 2018
 *      Author: dongnx
 */
#include "main.h"
#include "simcom.h"
#include "log.h"
#include "FreeRTOS.h"
#include "task.h"
#include "string.h"
static const char *TAG = "simcom";

static simcom_p simcom_proc = {
	simcom_dispose,
	simcom_reset,
	simcom_set_power,
	simcom_flight_mode,
	simcom_send_AT,
	simcom_send_data,
	simcom_receive,
	simcom_receive_ex,
	simcom_process};

static char sc_serial_number[16] = {0};
static char sc_fw_version[40] = {0};
static char sc_manufacturer_id[44] = {0};
static char sc_model_version[32] = {0};

static simcom_t sc_object = {0};

static void (*simcom_event_cb)(simcom_t * simcom, sc_evt_e event_id, void *param);
static int simcom_check_active(simcom_t *simcom);

 static void *memmem(const void *l, size_t l_len, const void *s, size_t s_len)
 {
 	register char *cur, *last;
 	const char *cl = (const char *)l;
 	const char *cs = (const char *)s;

 	/* we need something to compare */
 	if (l_len == 0 || s_len == 0)
 		return NULL;

 	/* "s" must be smaller or equal to "l" */
 	if (l_len < s_len)
 		return NULL;

 	/* special case where s_len == 1 */
 	if (s_len == 1)
 		return memchr(l, (int)*cs, l_len);

 	/* the last position where its possible to find "s" in "l" */
 	last = (char *)cl + l_len - s_len;

 	for (cur = (char *)cl; cur <= last; cur++)
 		if (cur[0] == cs[0] && memcmp(cur, cs, s_len) == 0)
 			return cur;

 	return NULL;
 }

uint32_t eat_get_current_time(void)
{
	return xTaskGetTickCount();
}
void osDelay(uint32_t Tick)
{
	vTaskDelay(Tick);
}
void eat_reset_module(void)
{

}
simcom_t *simcom_create(void)
{
	if (sc_object.proc == NULL)
	{
		sc_object.serial = embedded_at_create();
		sc_object.id = 0;
		sc_object.pending = false;
		sc_object.sleep_mode = 0;
		sc_object.serial_number = NULL;
		sc_object.cgns_version = NULL;
		sc_object.model = NULL;
		sc_object.fw_version = NULL;
		sc_object.vendor = NULL;
		sc_object.proc = &simcom_proc;
	}
	return &sc_object;
}

void simcom_dispose(simcom_t *object)
{

}

int simcom_reset(simcom_t *object, int mode)
{
	eat_reset_module();
	object->sleep_mode = 0;
	return object->sleep_mode;
}

int simcom_set_power(simcom_t *object, bool active)
{
	int count = 0;
	object->sleep_mode = -1;
	if (active)
	{
		object->pending = false;

		while ((count < 3) && (simcom_check_active(object) < 0))
		{
			count++;
			osDelay(1000);
			Logi( "Wait sim active %d time ...", count);
		}
		if (count >= 3)
		{
			Loge( "Simcom active");
			object->sleep_mode = -1;
			object->error_count = 0;
			osDelay(4000);
		}
		else
		{
			object->sleep_mode = 0;
			Logi( "Simcom on success");
		}
	}
	else
	{
		Logi( "Sim off success");
		return 0;
	}
	return object->sleep_mode;
}

int simcom_flight_mode(simcom_t *object, bool active)
{
	return -1;
}

int simcom_entry_sleep(simcom_t *object)
{
	uint8_t buffer[100];
	do
	{
		if (object->proc->send_AT(object, "+CFUN=0") < 0)
			break;
		object->sleep_mode = -1;
		object->proc->receive_ex(object, "+PDP: DEACT", 2000, buffer, 100);
		osDelay(3000);
	} while (0);
	return object->sleep_mode;
}

int simcom_exit_sleep(simcom_t *object)
{
	do
	{
		if (object->proc->send_AT(object, "+CFUN=1") < 0)
			break;
		object->pending = 0;
		object->sleep_mode = 0;
	} while (0);
	return object->sleep_mode;
}

int simcom_get_battery(simcom_t *object)
{
	int value = -2;
	uint8_t message[40];
	int message_size;

	if (simcom_send_AT(object, "+CBC") >= 0)
	{
		value = -1;

		message_size = simcom_receive(object, message, 40, 2000);

		if ((message_size > 0) && (message_size < 40))
		{
			int index;
			bool valid = false;

			for (index = 0; index < message_size; index++)
			{
				if ((message[index] == ',') && valid)
				{
					break;
				}
				if (valid)
				{
					value = value * 10;
					value += message[index] - 0x30;
				}
				if ((message[index] == ',') && !valid)
				{
					valid = true;
					value = 0;
				}
			}
		}
	}
	return value;
}

const char *simcom_get_manufacturer_id(simcom_t *object)
{
	if ((object->vendor == NULL) && (simcom_send_AT(object, "+CGMI") >= 0))
	{
		uint8_t message[100];
		int message_size = simcom_receive(object, message, 100, 3000);
		if ((message_size > 0) && (message_size < 48))
		{
			memcpy(sc_manufacturer_id, message, message_size);
			object->vendor = sc_manufacturer_id;
		}
	}
	return object->vendor;
}
const char *simcom_get_fw_version(simcom_t *object)
{
	if ((object->fw_version == NULL) && (simcom_send_AT(object, "+CGMR") >= 0))
	{
		uint8_t message[100];
		int message_size = simcom_receive(object, message, 100, 3000);
		if ((message_size > 0) && (message_size < 48))
		{
			memcpy(sc_fw_version, &message[9], message_size - 8);
			object->fw_version = sc_fw_version;
		}
	}
	return object->fw_version;
}

const char *simcom_get_model_id(simcom_t *object)
{
	if (object->model == NULL && (simcom_send_AT(object, "+CGMM") >= 0))
	{
		uint8_t message[45];
		int message_size = simcom_receive(object, message, 45, 3000);
		if ((message_size > 0) && (message_size < 48))
		{
			memcpy(sc_model_version, message, message_size);
			object->model = sc_model_version;
		}
	}
	return object->model;
}

const char *simcom_get_serial_number(simcom_t *object)
{
	if (object->serial_number == NULL && (simcom_send_AT(object, "+CGSN") >= 0))
	{
		uint8_t message[45];
		int message_size = simcom_receive(object, message, 45, 3000);
		if ((message_size > 0) && (message_size < 48))
		{
			memcpy(sc_serial_number, message, 15);
			object->serial_number = sc_serial_number;
		}
	}
	return object->serial_number;
}

bool simcom_wait_pwron(simcom_t *object, uint32_t timeout)
{
	uint32_t tick = eat_get_current_time();
	while (eat_get_current_time() - tick < timeout)
	{
		int val = simcom_check_active(object);
		if (val >= 0)
			return 1;
		osDelay(1000);
	}
	return 0;
}
bool simcom_wait_ready(simcom_t *object, uint32_t timeout)
{
	uint32_t entry = eat_get_current_time();
	uint32_t elapsed = 0;
	uint32_t remained = 0;
	while (1)
	{
		int val = simcom_check_active(object);
		if (val >= 0)
		{
			elapsed = eat_get_current_time() - entry;
			if (elapsed < timeout)
				remained = timeout - elapsed;
			break;

		}
		if (eat_get_current_time() - entry > timeout)
			return 0;
		osDelay(1000);
	}
	if (remained > 0)
	{
		entry = eat_get_current_time();
		uint8_t message[30];
		while (1)
		{
			if (simcom_receive_ex(object, "READY", 2000, message, 30) == 0)
				break;
			if (eat_get_current_time() - entry > remained)
				return 0;
		}
	}
	return 1;
}
int simcom_get_csq(simcom_t *object)
{
	if (simcom_send_AT(object,"+CSQ") >= 0)
	{
		uint8_t message[45];
		int message_size = simcom_receive(object, message, 45, 3000);
		if ((message_size > 0) && (message_size < 48))
		{
			char csq[4] = {0};
			memcpy(csq,&message[6],3);
			if (csq[2] == ',') csq[2] = '\0';
			// ESP_LOGI("csq:%s",csq);
			int signal = atoi(csq);
			return signal;
		} 
	}
	return 0;
}
uint32_t simcom_get_id(simcom_t *object)
{
	if ((object->id == 0) && (object->serial_number != NULL))
	{
		int size = strlen(object->serial_number);
		if (size >= 8)
		{
			object->id = object->serial_number[size - 8] - 0x30;
			object->id = (object->id * 10) + (object->serial_number[size - 7] - 0x30);
			object->id = (object->id * 10) + (object->serial_number[size - 6] - 0x30);
			object->id = (object->id * 10) + (object->serial_number[size - 5] - 0x30);
			object->id = (object->id * 10) + (object->serial_number[size - 4] - 0x30);
			object->id = (object->id * 10) + (object->serial_number[size - 3] - 0x30);
			object->id = (object->id * 10) + (object->serial_number[size - 2] - 0x30);
			object->id = (object->id * 10) + (object->serial_number[size - 1] - 0x30);
		}
	}
	return object->id;
}

int simcom_send_AT(simcom_t *object, const char *message)
{
	uint8_t packet[512];
	int size = 0;
	if (message != NULL) size = strlen(message);
	packet[0] = 'A';
	packet[1] = 'T';
	packet[size + 2] = '\r';
	if (size > 500)
		size = 500;
	if (size > 0 )
		memcpy(&packet[2], message, size);
	return simcom_send_data(object, (uint8_t *)packet, size + 3);
}


int simcom_send_data(simcom_t *object, uint8_t *data, int size)
{

	int reval = -1;
	if (object->pending)
	{
		Loge( "simcom pending...");
		return (-1);
	}
	object->serial->proc->flush();
	if (object->serial->proc->write_data(data, size))
	{
		object->pending = true;
		reval = 0;
	}
	return reval;
}

int simcom_receive(simcom_t *object, uint8_t *response, int size, int timeout)
{
	int index = 0;
	int message_size = -2;
	uint32_t tick;
	char *end_pos;
	char *start_pos;

	memset(response, 0, size);
	tick = eat_get_current_time();
	while (eat_get_current_time() - tick < timeout)
	{
		index += object->serial->proc->read_data(&response[index], 1);
		if ((strstr((const char *)response, "OK\r\n") != NULL) || (strstr((const char *)response, "ERROR\r\n") != NULL))
			break;
	}
#if SIM_DEBUG == 1
	printf("[%d]%s", index, response);
//	for (int i = 0; i < index ; i++)
//		printf("%.2x ", response[i]);
//	printf("\r\n");
#endif
	do
	{
		if (strstr((const char *)response, "OK\r\n") != NULL)
		{
			object->error_count = 0;
			message_size = 0;
			end_pos = strstr((const char *)response, "\r\n\r\nOK");
			if (end_pos == NULL)
			{
				end_pos = strstr((const char *)response, "\r\nOK");
			}
			if (end_pos == NULL)
			{
				break;
			}

			start_pos = strstr((const char *)response, "\r\n\r\n");
			if ((start_pos == NULL) || (start_pos >= end_pos))
			{
				start_pos = strstr((const char *)response, "\r\n");
				if (start_pos == NULL)
				{
					message_size = -1;
					break;
				}
				else
					start_pos += 2;
			}
			else
				start_pos += 4;

			if (end_pos > start_pos)
			{
				message_size = end_pos - start_pos;
				if (message_size > size)
					message_size = size;
				memcpy((void *)response, start_pos, message_size);
				response[message_size] = 0;
			}
			break;
		}
		else if (strstr((const char *)response, "ERROR\r\n") != NULL)
		{
			message_size = -1;
			break;
		}
	} while (0);

	if (message_size < 0)
	{
		object->error_count++;
		if (strlen((const char *)response) > 0)
			Logi( "Simcom response: (%d)%s", object->error_count, response);
		else 
			Logi( "Send ATC timeout: (%d)", object->error_count);
	}

	object->pending = false;
	return message_size;
}

int simcom_receive_ex(simcom_t *object, const char *expect, int timeout, uint8_t *buffer, int size)
{
	int index = 0;
	int reval = -1;
	uint32_t tick;
	memset(buffer, 0, size);

	tick = eat_get_current_time();
	while (eat_get_current_time() - tick < timeout)
	{
		index += object->serial->proc->read_data(&buffer[index], 1);
		if (memmem((const void*)buffer, size, (const void *)expect, strlen(expect)) != NULL)
		{
			object->error_count = 0;
			reval = 0;
			break;
		}
		reval = -1;
	}
#if SIM_DEBUG == 1
	printf("[%d]%s", index, buffer);
//	for (int i = 0; i < index ; i++)
//		printf("%.2x ", response[i]);
//	printf("\r\n");
#endif
	if (reval < 0)
		Logi("TAG", "Simcom response ex: (%d)%s", index, buffer);
	object->pending = false;
	return reval;
}

static int simcom_check_active(simcom_t *simcom)
{
	int value = -1;
	if (simcom->proc->send_AT(simcom,NULL) >= 0)
	{
		uint8_t message[30];
		value = simcom_receive(simcom, message, 30, 500);
	}
	return value;
}
void simcom_process(simcom_t * simcom)
{
	uint8_t buffer[1024] = {0};
	static TickType_t checkData = 0;
	int size;
	if (simcom == NULL)
		return;
	if (simcom->pending)
		return;
	if (simcom->serial->proc->get_data_rdy() > 10)
	{
		size = simcom->serial->proc->read_data_block(buffer, 1024);
		if (size <= 0)
			return;
		if (strstr((const char*)buffer,(const char*)"+CPIN: NOT READY\r\n") != NULL)
		{
			Logi( "SC_NO_SIMCARD_EVT");
			if (simcom_event_cb != NULL)
				simcom_event_cb(simcom, SC_NO_SIMCARD_EVT, NULL);
		}
		else if (strstr((const char*)buffer,(const char*)"CLOSED\r\n") != NULL)
		{
			Logi("SC_SOCK_CLOSE_EVT");
			if (simcom_event_cb != NULL)
				simcom_event_cb(simcom, SC_SOCK_CLOSE_EVT, NULL);
		}
		else if (strstr((const char*)buffer,(const char*)"+CIPRXGET:") != NULL)
		{
			checkData = xTaskGetTickCount();
			Logi("SC_SOCK_DATARDY_EVT");
			if (simcom_event_cb != NULL)
				simcom_event_cb(simcom, SC_SOCK_DATARDY_EVT, NULL);
		}
		else if (strstr((const char*)buffer,(const char*)"+PDP: DEACT\r\n") != NULL)
		{
			Logi("SC_PDP_DEACT_EVT");
		}
	}
	else
	{
		if( xTaskGetTickCount() - checkData > 2000)
		{
//			Logi( "Check data manually");
			checkData = xTaskGetTickCount();
			if (simcom_event_cb != NULL)
				simcom_event_cb(simcom, SC_SOCK_DATARDY_EVT, NULL);
		}
	}

}
void simcom_register_cb(void(*callback)(simcom_t * simcom, sc_evt_e event_id, void *param))
{
	simcom_event_cb = callback;
}

