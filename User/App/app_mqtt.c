/*
 * app_mqtt.c
 *
 *  Created on: 12 Jan 2023
 *      Author: manht
 */
#include "main.h"
#include "atc.h"
#include "log.h"
#include "stimer.h"
#include "simcom.h"
#include "network.h"
#include "socket.h"
#include "fifo.h"
#include "mqtt.h"

#include "FreeRTOS.h"
#include "task.h"
extern socket_t *sock;
extern simcom_t *sc;

static const char *TAG = "app_mqtt";
static const char *broker = "broker.hivemq.com";
static const int port = 1883;
static NetworkContext_t *networkCtx = NULL;
static TransportInterface_t *netif = NULL;
MQTT_t *client = NULL;
MQTTSubscribeInfo_t SubscribeInfo[5] = {0};
bool MQTT_Connected = false;
static void MQTTEventCallback(MQTTEvent_t *event)
{
    if (event->event_id == MQTT_EVENT_CONNECTED)
    {
        if (event->error_code == MQTTSuccess)
        {
            if (event->client == client)
            {
                Logi("mqtt client connect %s", FUNC_SUCCESS);
                SubscribeInfo[0].pTopicFilter = "topic/request";
                SubscribeInfo[0].qos = 0;
                SubscribeInfo[0].topicFilterLength = strlen("topic/request");
                int retry = 0;
                while (retry < 3)
                {
                	if (MQTTSubcribe(event->client, SubscribeInfo, 1) == MQTTSuccess)
                		break;
                	vTaskDelay(2000);
                }
            }
        }

        else if (event->error_code == MQTTServerRefused)
        	Logi("MQTT Server refused connection");
    }
    if (event->event_id == MQTT_EVENT_SUBSCRIBED)
    {
        int idx = event->topic_idx;
        int qos = event->error_code;
        Logi("subcribe topic \"%s\" %s", SubscribeInfo[idx].pTopicFilter, (qos == MQTTSubAckFailure) ? FUNC_FAIL : FUNC_SUCCESS);
        MQTT_Connected = true;
    }
    if (event->event_id == MQTT_EVENT_PUBLISHED)
    {
    	Logi("publish successed %d", event->qos);
    }
    if (event->event_id == MQTT_EVENT_DATA)
    {
        if ((event->topic != NULL) && (event->data != NULL) )
        {
            Logi("topic(%d): %s", event->topic_len, event->topic);
            Logi("payload(%d): %s", event->data_len, event->data);
            free(event->topic);
            free(event->data);
        }

    }
    if (event->event_id == MQTT_EVENT_DISCONNECTED)
    {
        if (event->error_code == -1 )
        {
            Loge("socket disconnected, try to open socket again and send CONNECT packet");
            while (sock->proc->open(sock, broker, port) < 0)
            {
            	vTaskDelay(5000);
            }
        }
    }
}
void AppMqttLoop(void *param)
{
	TickType_t xLastWakeTime = xTaskGetTickCount();
	const TickType_t xFrequency = 100;
	TickType_t pubEntry = xTaskGetTickCount();
	while (1)
	{
		MQTTProcess(client);
		vTaskDelayUntil(&xLastWakeTime, xFrequency);
		if (MQTT_Connected && xTaskGetTickCount() - pubEntry > 5000)
		{
			pubEntry = xTaskGetTickCount();
            char *payload = "SIM800C Client";
            if (MQTTPublish(client, "topic/response", payload, strlen(payload), 0, 0) == MQTTSuccess)
            	Logi("Publish message %s",FUNC_SUCCESS);
		}
	}
}
void AppMqttStart(void)
{
	int success = -1;
    do {
    	sock = socket_create_tcp(sc, 0);
    	if (sock == NULL)
    		break;
    	networkCtx = network_ctx_create(sock);
        if (networkCtx == NULL)
        	break;
        netif = transport_if_create(networkCtx);
        if (netif == NULL)
        	break;
        client = MQTTInit(netif);
        if (client == NULL)
        	break;
        if (client == NULL)
        	break;
        if (sock->proc->open(sock, broker, port) < 0)
        	break;
        MQTTConnectInfo_t ConnectInfo = {
        		.cleanSession = true,
				.pClientIdentifier = "SIM800C",
				.clientIdentifierLength = strlen("SIM800C"),
				.keepAliveSeconds = 120,
				.pUserName = "unused",
				.userNameLength = strlen(ConnectInfo.pUserName),
				.pPassword = "unused",
				.passwordLength = strlen(ConnectInfo.pPassword),
        };
        MQTTRegisterCallback(MQTTEventCallback);
        do {
            if (MQTTStart(client, &ConnectInfo) == MQTTSuccess)
            	break;
            vTaskDelay(5000);
        } while (1);
        if (xTaskCreate(AppMqttLoop, "AppMqttLoop", 500, NULL, configMAX_PRIORITIES - 3, NULL) != pdTRUE)
        	break;
        success = 0;
    } while(0);
    if (success == -1)
    	NVIC_SystemReset();
    Logi("start mqtt client %s", (success < 0) ? FUNC_FAIL : FUNC_SUCCESS);

}
