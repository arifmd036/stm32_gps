/*
 * gnss.c
 *
 *  Created on: Mar 9, 2023
 *      Author: manht
 */
#include "main.h"
#include "log.h"
#include "gps_type.h"
#include "lwgps.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "string.h"
#define FRAME_SIZE	256
#define SLAVE 	1
typedef struct
{
	uint32_t tick;
	uint8_t buffer[FRAME_SIZE];
	int size;
} EventData_t;

uint8_t gps_rx = 0;

uint8_t RxBuf[FRAME_SIZE] = {0};
static EventData_t EvtData = {0};
static QueueHandle_t EventQueueHandle = NULL;

__IO int fix_gps = 0;
__IO int start_measure = 0;
int start_timer = 0;
__IO int start_phaseA = 0;
uint16_t phaseA_time[256] = {0};
uint16_t phaseB_time[256] = {0};
uint16_t phaseC_time[256] = {0};

uint16_t phaseC_data[256] = {0};
int phaseA_cnt = 0;
int phaseB_cnt = 0;
int phaseC_cnt = 0;
__IO uint32_t tick = 0;

uint16_t offset[3] = {0};
static uint32_t lastTick[3] = {0};
lwgps_t nmea;
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	if (GPIO_Pin == PULSE_Pin) {
		if (!fix_gps)
			fix_gps = true;
		if (start_measure && !start_timer) {
			tick = 0;
			lastTick[0] = 0;
			lastTick[1] = 0;
			lastTick[2] = 0;
			HAL_TIM_Base_Start_IT(&htim5);
//			HAL_NVIC_EnableIRQ(EXTI3_IRQn);
			start_timer = 1;
			start_phaseA = 0;
		}
	}
	else if (GPIO_Pin == PHASEA_Pin) {
#if (SLAVE == 1)
		if (start_timer == 1) {
			phaseA_time[phaseA_cnt++] = (uint16_t)(tick - lastTick[0]);
			lastTick[0] = tick;
		}
#else
		if (!start_phaseA)
			start_phaseA = 1;
		if (start_timer == 1) {
			phaseA_time[phaseA_cnt++] = (uint16_t)(tick - lastTick[0]);
			lastTick[0] = tick;
		}
#endif
	}
	else if (GPIO_Pin == PHASEB_Pin) {
#if (SLAVE == 1)
		if (start_timer == 1) {
			phaseB_time[phaseB_cnt++] = (uint16_t)(tick - lastTick[1]);
			lastTick[1] = tick;
		}
#else
		if (start_timer == 1 && start_phaseA) {
			phaseB_time[phaseB_cnt++] = (uint16_t)(tick - lastTick[1]);
			lastTick[1] = tick;
		}
#endif
	}
	else if (GPIO_Pin == PHASEC_Pin) {
#if (SLAVE == 1)
		if (start_timer == 1) {
			phaseC_time[phaseC_cnt++] = (uint16_t)(tick - lastTick[2]);
			lastTick[2] = tick;
		}
#else
		if (start_timer == 1 && start_phaseA) {
			phaseC_time[phaseC_cnt++] = (uint16_t)(tick - lastTick[2]);
			lastTick[2] = tick;
		}
#endif
	}
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size){
	BaseType_t xHigherPriorityTaskWoken  = pdFALSE;
	HAL_UARTEx_ReceiveToIdle_DMA(&huart4, (uint8_t *) RxBuf, FRAME_SIZE);
	EvtData.size = Size;
	EvtData.tick = HAL_GetTick();
	memcpy(EvtData.buffer, RxBuf, Size);
	if (EventQueueHandle != NULL)
	{
		xQueueSendFromISR(EventQueueHandle, (void*)&EvtData, &xHigherPriorityTaskWoken);
	}
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}


void AppGNSSStart(void){
	Logi("Power on GNSS Module");
	HAL_GPIO_WritePin(PWR_GPS_GPIO_Port, PWR_GPS_Pin, 0);
	HAL_Delay(1000);
	uint8_t *cmd = (uint8_t*)"$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n";
	HAL_UART_Transmit(&huart4, cmd, strlen((char*)cmd), 1000);
	HAL_Delay(1000);
	cmd = (uint8_t*)"$PMTK251,115200*1F\r\n";
	HAL_UART_Transmit(&huart4, cmd, strlen((char*)cmd), 1000);
	HAL_UART_AbortReceive(&huart4);
	HAL_UART_DeInit(&huart4);
	huart4.Init.BaudRate = 115200;
	lwgps_init(&nmea);
	if (HAL_UART_Init(&huart4) != HAL_OK) {
	    while(1);
	}
	while(!fix_gps){
		;
	}
//	HAL_NVIC_DisableIRQ(EXTI3_IRQn);
	HAL_Delay(2000);
	start_measure = true;
	Logi("GPS Fix, start measure");
	HAL_UART_Receive_IT(&huart4, &gps_rx, 1);
	while (1) {
		if (phaseA_cnt == 15 || phaseB_cnt == 15 || phaseC_cnt == 15) {
			  phaseA_cnt = 0;
			  phaseB_cnt = 0;
			  phaseC_cnt = 0;
			  HAL_TIM_Base_Stop_IT(&htim5);
			  start_timer = false;
			  offset[0] = phaseA_time[0];
			  offset[1] = phaseB_time[0];
			  offset[2] = phaseC_time[0];
			  memset(phaseA_time, 0, sizeof(phaseA_time));
			  memset(phaseB_time, 0, sizeof(phaseB_time));
			  memset(phaseC_time, 0, sizeof(phaseC_time));
//			  printf("Sample data on phaseC: [%02d:%02d:%02d] ", nmea.hours, nmea.minutes, nmea.seconds);
//			  for (int i = 0; i < 10; i++)
//				  printf("%d, ", phaseC_data[i] * 100);
//			  printf("\r\n");

		}
		if (is_end) {
			is_end = false;
			lwgps_process(&nmea, data.data, data.size);
			printf("Sample data on phase: [%02d:%02d:%02d] %d %d %d \r\n", nmea.hours, nmea.minutes, nmea.seconds,
					offset[0], offset[1], offset[2]);
		}
	}
}

